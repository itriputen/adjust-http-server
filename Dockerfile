FROM python:3

RUN useradd -ms /bin/bash http-server

COPY server.py ./

EXPOSE 80

USER http-server

CMD [ "python", "./server.py" ]