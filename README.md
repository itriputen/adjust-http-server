# http_server

## prerequisites

1. minikube
2. git
3. helm v3
4. kubectl
5. docker

## start

`./up.sh`

## clean up

`./down.sh`