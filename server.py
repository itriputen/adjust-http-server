#!/usr/bin/env python3
"""
Very simple HTTP server in python for logging requests
Usage::
    ./server.py [<port>]
"""
from http.server import BaseHTTPRequestHandler, HTTPServer
import logging

class S(BaseHTTPRequestHandler):
    def _set_response(self, encode):
        self.send_response(200)
        self.send_header('Content-type', encode)
        self.end_headers()

    def do_GET(self):
        if self.path == '/':
           self._set_response('text/html')
           self.wfile.write("Well, hello there!".encode("utf-8"))

        if self.path == '/healthcheck':
           self._set_response('application/json')
           self.wfile.write("OK".encode("utf-8"))


def run(server_class=HTTPServer, handler_class=S, port=80):
    logging.basicConfig(level=logging.INFO)
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    logging.info('Starting httpd...\n')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')

if __name__ == '__main__':
    from sys import argv

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()

        