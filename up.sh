#!/bin/bash

minikube start

minikube addons enable metrics-server # for HPA needs

eval $(minikube docker-env)

docker build -t my-ruby-app:latest .

helm install http-server ./http-server

sleep 10 # time for service to start

minikube service http-server-myruby